FROM cabaleirog/crawler:0.1.0

ENV JEFIT_USERNAME=your_google_username_here
ENV JEFIT_PASSWORD=your_google_password_here

COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

WORKDIR /app

COPY ./src /app

CMD ["python", "spider.py"]
