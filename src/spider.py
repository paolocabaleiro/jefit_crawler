import getpass
import logging
import os
import re
import time
from datetime import datetime
from urllib.request import urlretrieve

from selenium.webdriver import PhantomJS
from selenium.webdriver.remote.errorhandler import NoSuchElementException
from selenium.webdriver.remote.remote_connection import LOGGER

from crawler import SeleniumParser  # Found on docker image cabaleirog/crawler
from pymongo import MongoClient

LOGGER.setLevel(logging.WARNING)  # Selenium logger

logging.basicConfig(
    level=logging.DEBUG,
    format='%(levelname)-8s %(asctime)s - %(name)s: %(message)s')

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


BASE_URL = 'https://www.jefit.com'
LOGIN_URL = '{}/login/'.format(BASE_URL)
ROUTINES_URL = '{}/routines'.format(BASE_URL)

WAIT_DELAY = 10


def get_jetit_routines(web_driver_cls=PhantomJS):
    """Extract custom routines information from Jefit's website.

    Jefit is a mobile app for tracking workout details and progress.

    `JEFIT_USERNAME` and `JEFIT_PASSWORD` can be defined as enviroment
    variables, otherwise, these inputs will be requested on startup.

    Arguments:
    ----------
    web_driver_cls: Selenium WebDriver class (default PhantomJS)
    """

    # Credentials (Only supports Google authentication as of now)
    username = os.getenv('JEFIT_USERNAME')
    if not username:
        username = input('Username (Google): ')

    password = os.getenv('JEFIT_PASSWORD')
    if not password:
        password = getpass.getpass('Password: ')

    # Use any Selenium webdriver to crawl the site
    driver = web_driver_cls()

    # Go directly to the login page
    driver.get(LOGIN_URL)

    # Select Google as the Authentication method
    google_login = driver.find_element_by_xpath(
        '//a[@class="googleLoginButton"]')
    google_login.click()
    time.sleep(WAIT_DELAY)  # Let the login form load completely

    # Authenticate with Google
    logger.info('Signing in using username <%s>', username)
    try:
        login_using_google_phone_or_email(driver, username, password)
    except NoSuchElementException:
        login_using_google_email_only(driver, username, password)

    # Navigate into the routines page, then into Elite (Premium user required)
    driver.find_element_by_xpath('//a[contains(@href, "routines")]').click()
    driver.find_element_by_xpath('//a[@href="?elite"]').click()

    # Use the pagination text to know how many pages we need to crawl
    pages = driver.find_element_by_xpath(
        '//div[@class="pageCell"][contains(text(), "Page 1 of")]').text
    total_pages = int(re.search(r'\d+$', pages).group())

    current_page = 1
    while current_page <= total_pages:
        logger.info('Crawling page %s out of %s', current_page, total_pages)

        if current_page > 1:
            url = '{}/?name=1&tag=1&keyword=0&sort=3&elite&page={}'.format(
                ROUTINES_URL, current_page)
            driver.get(url)  # Navigate to the following routine's list page

        # Get a list with all URLs for future navigation
        rows = driver.find_elements_by_xpath(
            '//tr//a[contains(@href, ".php?id")]')
        urls = list(set([row.get_attribute('href') for row in rows]))
        logger.debug('%s routines found on page %s', len(urls), current_page)

        for url in urls:
            driver.get(url)  # Navigate into the single routine's page
            routine = parse_routine(driver)  # Extract routine's data

            # Get routine image (Top banner)
            image_url = routine.get('image', None)
            if image_url:
                image_url = re.search(r'image.*', image_url).group()
                routine_id = routine.get('routine_id')
                image_path = '/tmp/routine_{}.jpg'.format(routine_id)

                # ToDo: Save the image on MongoDB
                urlretrieve(
                    url='{}/{}'.format(BASE_URL, image_url),
                    filename=image_path)

                logger.debug('Banner image stored at %s', image_path)

            logger.debug('Routine %s crawled successfully', routine['title'])
            add_routine_to_db(routine)

        current_page += 1

    # Clean-up
    username = None
    password = None
    driver.quit()


def parse_routine(driver):
    """Parse the Routine page and return a dictionary.

    Arguments:
    ----------
    driver -- The Selenium WebDriver instance
    """
    parser = SeleniumParser(driver)

    parser.add_xpath_rule(
        'title',
        '//title/text()',
        regex=r'^(?:Elite - )?(.*?)\|')

    parser.add_xpath_rule(
        'description',
        '//td[contains(strong, "Description")]//*/text()',
        as_list=True)

    parser.add_xpath_rule(
        'image',
        '//img[contains(@src, "banner")]/@src')

    parser.add_xpath_rule(
        'shared_by',
        '//p[strong[contains(text(), "Shared By")]]/a/text()')

    parser.add_xpath_rule(
        'shared_by_url',
        '//p[strong[contains(text(), "Shared By")]]/a/@href')

    parser.add_xpath_rule(
        'rating_average',
        '//div[@id="star"]//div[@id="starUser0"]/text()')

    parser.add_xpath_rule(
        'rating_number_of_votes',
        '//p[contains(text(), "Based upon")]/text()',
        regex=r'(\d+)')

    # ToDo: Try to find a better way to extract this field using XPath
    # The issue here is that the text doesn't have a tag.
    parser.add_xpath_rule(
        'information',
        '//strong[text()="Information"]/../..//text()',
        as_list=True)

    info = parser.parse()

    # Remove description blank lines
    if info['description']:
        info['description'] = [x for x in info['description'] if x]

    # Extract data from the information field, we want the values at the
    # bottom of the list
    aux = list(reversed(info.pop('information')))
    while len(aux) > 1:  # We need at least one pair
        if aux[0].strip() == '':  # Stop at the first blank line
            break
        value, key = aux[:2]
        key = key.lower().replace(':', '').strip().replace(' / ', '/')
        info[key] = value
        aux = aux[2:]

    routine_table_xpath = './/table[@id="hor-minimalist_3"]'
    rutine = []
    for day in driver.find_elements_by_xpath(routine_table_xpath):
        day_parser = SeleniumParser(day)

        day_parser.add_xpath_rule(
            'day',
            './/tr[1]//td[1]/text()')

        day_parser.add_xpath_rule(
            'day_title',
            './/tr[1]//td[2]//text()')

        # Get all excercises for the day
        excercise_table_xpath = './/tr[2]//table//tr[position()>1]'
        exercises = []
        excercises_selector = day.find_elements_by_xpath(excercise_table_xpath)
        for i, excercise in enumerate(excercises_selector):
            rutine_parser = SeleniumParser(excercise)

            rutine_parser.add_xpath_rule(
                'muscle',
                './/td[1]//text()')

            rutine_parser.add_xpath_rule(
                'excercise_url',
                './/td[3]//a/@href')

            rutine_parser.add_xpath_rule(
                'excercise_name',
                './/td[3]//a/text()')

            rutine_parser.add_xpath_rule(
                'timer',
                './/td[4]//text()')

            rutine_parser.add_xpath_rule(
                'reps',
                './/td[5]//text()')

            rutine_parser.add_xpath_rule(
                'sets',
                './/td[6]//text()')

            parsed = rutine_parser.parse()  # Single excercise on one day
            parsed['order'] = i
            exercises.append(parsed)

        parsed = day_parser.parse()  # Routine for one day
        parsed['exercises'] = exercises  # Excercises on that day
        rutine.append(parsed)

    info.update({'rutine': rutine})

    # Routine's URL
    info['routine_url'] = driver.current_url

    # Routine's ID
    info['routine_id'] = re.search(r'id=(\d+)', driver.current_url).group(1)

    # Date of crawling
    info['crawled_at'] = datetime.now()

    return info


def login_using_google(driver, username, password, mapping):
    """Generic Google Log-in using XPath mapping.

    Arguments:
    ----------
    driver -- The Selenium WebDriver instance
    username -- The Google username used to log into Jefit
    password -- The Google password
    mapping -- A dictionary of XPath selectors
    """

    # Filling Google's username screen
    login_input = driver.find_element_by_xpath(mapping['login'])
    login_input.send_keys(username)
    button_next = driver.find_element_by_xpath(mapping['login-btn'])
    button_next.click()
    time.sleep(WAIT_DELAY)  # Let the password form load completely

    # Uncheck the 'Stay signed in' checkbox
    if mapping.get('checkbox', None):
        checkbox = driver.find_element_by_xpath(mapping['checkbox'])
        if checkbox.is_selected:
            checkbox.click()

    # Filling Google's password screen
    password_input = driver.find_element_by_xpath(mapping['password'])
    password_input.send_keys(password)
    button_next = driver.find_element_by_xpath(mapping['password-btn'])
    button_next.click()
    time.sleep(WAIT_DELAY)


def login_using_google_email_only(driver, username, password):
    """Log-in method used when Google shows Email Authentication."""
    mapping = {
        'login': '//input[@id="identifierId"]',
        'login-btn': '//span[text()="Next"]',
        'password': '//input[@name="password"]',
        'password-btn': '//span[text()="Next"]',
    }

    login_using_google(driver, username, password, mapping)
    logger.info("Successfully logged in using Google's Email only form")


def login_using_google_phone_or_email(driver, username, password):
    """Log-in method used when Google shows Email or Phone Authentication."""
    mapping = {
        'login': '//input[@id="Email"]',
        'login-btn': '//*[@type="submit"]',
        'password': '//input[@type="password"]',
        'password-btn': '//input[@id="signIn"]',
        'checkbox': '//input[@id="PersistentCookie"]',
    }

    login_using_google(driver, username, password, mapping)
    logger.info("Successfully logged in using Google's Email/Phone form")


def add_routine_to_db(routine):
    """Save the routine into MongoDB."""
    client = MongoClient('mongodb://mongodb:27017/')
    collection = client.jefit.routines

    # Check if the routine is already in the database
    if not collection.find_one({'routine_id': routine['routine_id']}):
        collection.insert_one(routine)
        logger.debug('Routine %s inserted', routine['routine_id'])
    else:
        # ToDo: Check if the routine is already saved before crawling
        # ToDo: Add a variable to allow updating a routine if already in the DB
        logger.info('Routine %s already in database', routine['routine_id'])

    client.close()

if __name__ == '__main__':
    get_jetit_routines(PhantomJS)
